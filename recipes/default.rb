#
# Cookbook Name:: hbo_tomcat
# Recipe:: default
#
# Copyright (C) 2016 Henry Medina
#
# All rights reserved - Do Not Redistribute
#

packages = ['epel-release', 'java-1.8.0-openjdk', 'nginx', 'tomcat']

packages.each do |pkg|
  package pkg
end

service 'tomcat' do
   action [ :enable, :start ]
end

service 'nginx' do
   action [ :enable, :start ]
end

template '/etc/nginx/conf.d/tomcat-proxy.conf' do
  source 'tomcat_proxy_conf.erb'
  notifies :restart, 'service[nginx]'
end

