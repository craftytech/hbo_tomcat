# hbo_tomcat-cookbook

TODO: Enter the cookbook description here.

## Supported Platforms

TODO: List your supported platforms.

## Attributes

<table>
  <tr>
    <th>Key</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
  </tr>
  <tr>
    <td><tt>['hbo_tomcat']['bacon']</tt></td>
    <td>Boolean</td>
    <td>whether to include bacon</td>
    <td><tt>true</tt></td>
  </tr>
</table>

## Usage

### hbo_tomcat::default

Include `hbo_tomcat` in your node's `run_list`:

```json
{
  "run_list": [
    "recipe[hbo_tomcat::default]"
  ]
}
```

## License and Authors

Author:: Henry Medina (henry.medina@hbo.com)
